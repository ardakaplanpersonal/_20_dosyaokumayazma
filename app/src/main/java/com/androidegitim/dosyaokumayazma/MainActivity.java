package com.androidegitim.dosyaokumayazma;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private final int PERMISSION_REQUEST_STORAGE = 1;

    private File folderFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "UYGULAMA");

    private File file = new File(folderFile, "text.txt");

    @BindView(R.id.main_edittext_text)
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);
    }


    @OnClick(R.id.main_button_read)
    public void read() {

        try {

            StringBuilder textStringBuilder = new StringBuilder();

            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            String linetext;

            while ((linetext = bufferedReader.readLine()) != null) {

                textStringBuilder.append(linetext);

                textStringBuilder.append('\n');

            }

            bufferedReader.close();

            editText.setText(textStringBuilder);

        } catch (Exception e) {

            e.printStackTrace();

            Toast.makeText(getApplicationContext(), "Dosyayı okuma sırasında bir hata oluştu, lütfen tekrar deneyin", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.main_button_write)
    public void askPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {

                    DialogHelpers.showDialog(MainActivity.this, "Dosya Yazmak İçin Gerekli!", "Cihazın depolama alanına erişmek için bu izin gereklidir.",
                                             "Tamam", new DialogInterface.OnClickListener() {

                                @TargetApi(Build.VERSION_CODES.M)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);
                                }
                            }, "Hayır", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    Toast.makeText(getApplicationContext(), "İzin vermediniz.", Toast.LENGTH_SHORT).show();
                                }
                            }, null, null);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_STORAGE);
                }

            } else {

                write();
            }
        } else {

            write();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_STORAGE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            write();
        }
    }

    private void write() {

        String text = editText.getText().toString().trim();

        if (!folderFile.exists()) {

            boolean result = folderFile.mkdirs();

            RDALogger.info("folderFile.mkdirs() " + result);
        }


        try {

            boolean result = file.createNewFile();

            RDALogger.info("file.createNewFile() " + result);

            FileOutputStream fileOutputStream = new FileOutputStream(file);

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);

            outputStreamWriter.append(text);

            outputStreamWriter.close();

            fileOutputStream.flush();

            fileOutputStream.close();

            RDALogger.info("Dosya yolu " + file.getAbsolutePath());

        } catch (IOException e) {

            e.printStackTrace();

            Toast.makeText(getApplicationContext(), "Dosyaya yazma sırasında bir hata oluştu, lütfen tekrar deneyin", Toast.LENGTH_LONG).show();
        }
    }
}